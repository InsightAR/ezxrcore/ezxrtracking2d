# EZXR Tracking2d

EZXR Tracking2d 易现图片检测跟踪 Unity Package manager。

## 对应版本

| Unity Version | EZXR Tracking2d |
| ------------- | ----------- |
| 2022.3        | 0.1.1       |

## Release Note

### V0.1.0

1. 提供iOS/android EZXR Tracking2d 易现图片检测跟踪 SDK
2. 提供C#调用算法接口Tracking2dController类



## API 说明
